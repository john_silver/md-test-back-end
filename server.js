var compression = require('compression');
var express = require('express');

var path = require('path');
var logger = require('morgan');
var http = require('http');
var cors = require('cors');

var mongoose = require('mongoose');
require('mongoose-double')(mongoose);



/*var fs = require('fs');*/

// var multer  = require('multer');
// var upload = multer({ dest: 'uploads/' });

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var async = require('async');

var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

// добавление пользователя;
// var User = require('./models/User');
// var user = new User({
//     idToEnter : '16MD00',
//     email : 'alecksepyro@mail.ru',
//     password : '16MD00'
// });
// user.save(function (err) {
//     console.log('success');
// });


// cron function
require('./cron/closeTest');

mongoose.connect('mongodb://127.0.0.1:27017/mdTest');

var app = express();

// Middlewares
app.use(cors());
app.set('port', process.env.PORT || 8085); // для help desk
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(session({ secret: 'some key',
    resave:  true,
    saveUninitialized: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection })
}));

// Use of Passport user after login
app.use(function(req, res, next) {
    if (req.user) {
        res.cookie('user', JSON.stringify(req.user));
    }
    next();
});

app.use(require('./routes/auth.js'));
app.use(require('./routes/user.js'));
app.use(require('./routes/question.js'));
app.use(require('./routes/test.js'));
app.use(require('./routes/task.js'));
app.use(require('./routes/userTest.js'));
app.use(require('./routes/attachment.js'));


var testBase = {
    name : 'Вариант A',
    testA:[
        {
            type: 'table',
            title : 'Задача 1',
            description : 'Типовой план счетов бухгалтерского учета',
            price : '5000 тенге',
            questions : [
                {
                    type : 'table',
                    question : '',
                    answer : '',
                    columns : ['','Денежные средства в пути']
                },
                {
                    type : 'table',
                    question : '',
                    answer : '',
                    columns : ['','Краткросрочные банковские  займы']
                },
                {
                    type : 'table',
                    question : '',
                    answer : '',
                    columns : ['3150','']
                }
            ],
            columns : ['№ счета', 'Наименование счета']
        },
        {
            type: 'theory',
            title : 'Задача 2',
            description : 'Дайте полное разъяснение',
            price : '5000 тенге',
            questions : [
                {
                    type : 'text',
                    question : 'Что такое корреспонденция счетов?',
                    answer : ''
                },
                {
                    type : 'text',
                    question : 'Каков минимальный расчетный   показателей за 2016 год?',
                    answer : ''
                },
                {
                    type : 'text',
                    question : 'Двойная запись –это способ? ',
                    answer : ''
                }
            ]
        },
        {
            type: 'theory',
            title : 'Задача 3',
            description : 'Выберите правильный вариант ',
            price : '5000 тенге',
            questions : [
                {
                    type : 'test',
                    question : 'Накопление в кассе наличных денежных средств сверх установленного лимита допускается в случаях',
                    answers : [
                        'a.	В дни выплат заработной платы; в выходные, нерабочие праздничные дни в случае ведения юридическим лицом в эти дни кассовых операций;',
                        'b.	Только в выходные, нерабочие праздничные дни в случае ведения юридическим лицом в эти дни кассовых операций;',
                        'c.	Только в дни выплат заработной платы ;',
                        'Не допускается'
                    ]

                },
                {
                    type : 'test',
                    question : 'На основание  какого документа начисляется заработная плата сотруднику?',
                    answers : [
                        'Приказу директора;',
                        'Заявлению сотрудника;',
                        'Табель учета рабочего времени;',
                        'Трудового договора.'
                    ]
                },
                {
                    type : 'test',
                    question : 'Максимальный размер начислении заработной платы по больничному листу?',
                    answers : [
                        '15 МЗП',
                        '15 МРП',
                        '10 МЗП',
                        '10 МРП'
                    ]
                }
            ]
        },
        {
            type: 'theory',
            title : 'Бонусные задачи.',
            description : '',
            price : '5000 тенге',
            questions : [
                {
                    type : 'test',
                    question : 'Получение денежных средств с расчетного счета в кассу отражается',
                    answers : [
                        'Д 1250 «Расчеты с подотчетными лицами»  К 1010 «Касса»',
                        'Д 1010 «Касса»  К 1030 «Расчетные счета»',
                        'Д 1030 «Расчетные счета»  К 1010 «Касса»;',
                        'Д 1010 «Касса» К 1250 «Расчеты с подотчетными лицами».'
                    ]
                },
                {
                    type : 'test',
                    question : 'Работнику за май 2015 года начислена заработная плата в размере 200 000 тенге. Применяются следующие налоговые вычеты, кроме обязательного: сумма МЗП, сумма 30 000 тенге, направленная на погашения вознаграждения по суммам займа в Жилстройсбербанк. Сумма ИПН за май с доходов работника составит:',
                    answers : [
                        '17 714 тенге;',
                        '14 864 тенге;',
                        '15 714 тенге;',
                        '12 864 тенге.'
                    ]
                },
                {
                    type : 'test',
                    question : 'Каков смысл корреспонденции счетов: Дт «Готовая продукция» Кт  «Основное производства»',
                    answers : [
                        'Отпущены в производство материалы;',
                        'Получена из производства готовая продукция',
                        'Отгружена готовая продукция',
                        'Отпущены в производство готовая продукция'
                    ]
                }
            ]
        }
    ],
    testB:[
        {
            type: 'practical',
            title : 'Задача № 1.',
            description : '01.03.2015 г. ТОО «А» отпустило представителю ТОО «Б» (БИН 101040003344) Утенбаева Н.А.. по доверенности № 3 от 01.03.2015г Порошковая проволка в количестве 15 штук на общую сумму 312 885 тенге. \n Номер накладной на отпуск товара: 10'+
            'Заведующий складом ТОО «А» - Курманбаева Э. К.\n' + 'Главный бухгалтер – Не предусмотренn\n'+
            'Директор – Апраимова К. А.\n ТОО «Ваша Компания» не сдает 300.00 форму',
            price : '1500 тенге',
            questions : [
                {
                    type : 'text',
                    question : 'Что является подтверждением, что по данной накладной товар получен?',
                    answer : '',
                    price : '500 тенге'
                },
                {
                    type : 'text',
                    question : 'Какой дополнительный документ необходим, если товар был проплачен безналичным расчетом?',
                    answer : '',
                    price : '500 тенге'
                }
            ]
        },
        {
            type: 'practical',
            title : 'Задача № 2.',
            description : 'ТОО «А» оприходовало “Порошковая проволка”  в количестве  15 штук  на сумму 205 000 тенге, на основании накладной на отпуск товара от ТОО «В»(БИН 130140150160  ) № 13 от 25.02.2016 г.',
            price : '1500 тенге',
            questions : [
                {
                    type : 'text',
                    question : 'Перечислить какие документы от поставщика товаров являются основанием для прихода товаров: ',
                    answer : '',
                    price : '500 тенге'
                },
                {
                    type : 'text',
                    question : 'Чем отличаются товары от материалов? ',
                    answer : '',
                    price : '500 тенге'
                }
            ]
        },
        {
            type: 'practical',
            title : 'Задача №3.',
            description : '30.04.2016 г. ТОО «А» произвело ремонт автомобиля марки «MAZDA» для ТОО «Г» (БИН 140240000213) на сумму 80 000 тенге.\n\nНомер акта на выполненные работы: 14',
            price : '1500 тенге',
            questions : [
                {
                    type : 'text',
                    question : 'Что является подтверждением что работа выполнена? ',
                    answer : '',
                    price : '500 тенге'
                },
                {
                    type : 'text',
                    question : 'Когда компания имеет право взять в доход сумму по акту?',
                    answer : '',
                    price : '500 тенге'
                }
            ]
        }
    ]
};

var Question = require('./models/Question');
var Task = require('./models/Task');
var Test = require('./models/Test');

function addTest() {
    // console.log(testBase.testA);
    var test = new Test({
        name : testBase.name,
        testA : [],
        testB : []
    });

    testBase.testA.forEach(function (item) {
        var task = new Task({
            type : item.type,
            title : item.title,
            description: item.description,
            price: item.price,
            questions : [],
            columns : item.columns
        });
        item.questions.forEach(function (quest) {
            var question = new Question({
                type : quest.type,/** table, text, test, file */
                question : quest.question,
                answer : quest.answer,
                answers : quest.answers,
                columns : quest.columns
            });
            task.questions.push(question);
            question.save(function (err) {
                if (err)console.log(err);
            });
            console.log(question);
        });
        task.save(function (err) {
            if (err)console.log(err);
        });
        // console.log(task);
        test.testA.push(task);
    });

    testBase.testB.forEach(function (item) {
        var task = new Task({
            type : item.type,
            title : item.title,
            description: item.description,
            price: item.price,
            questions : []
        });
        item.questions.forEach(function (quest) {
            var question = new Question({
                type : quest.type,/** table, text, test, file */
                question : quest.question,
                answer : quest.answer,
                answers : quest.answers,
                columns : quest.columns
            });
            task.questions.push(question);
            question.save(function (err) {
                if (err)console.log(err);
            });
            console.log(question);
        });
        task.save(function (err) {
            if (err)console.log(err);
        });
        // console.log(task);
        test.testB.push(task);
    });

    test.save(function (err) {
        if (err)console.log(err);
        console.log('success');
    });
}

// addTest();

// console.log(test.testA);

app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.send(500, { message: err.message });
});

// Start server
var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});

