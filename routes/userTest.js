var express = require('express');
var async = require('async');
var nodemailer = require('nodemailer');
var config = require('config');

var app = express();

var UserTest = require('../models/UserTest');
var Test = require('../models/Test');
var Task = require('../models/Task');
var Question = require('../models/Question');

function sendEmail(item) {
    var html = '';
    console.log(item);
    html = '<div>' +
        '<div style="text-align: center;">' +
        '<img src="http://invoice.mirusdesk.kz/images/logo.png"><br><br><br>' +
        'Уведомление для "'+item.nameStudent+'"<br>' +
        'Уведомление<br>'+
        'о прохождении тестрования</div><br><br>'+
        'Уважаемый '+item.nameStudent+'</div><br>'+
        'Мы рады вам предоставить новую версию системы MD Test 1.0<br>'+
        'и для этого мы высылаем ссылку, расположенную ниже, по которой вы можете пройти тест <br>'+
        '<a href="'+ item.link+'">Ссылка для прохождения теста</a><br><br>'+
        '<br>'+
        'Внимание!<br>'+
        'После перехода по ссылке начинается время сдачи теста<br>'+
        'На прохождение теории отводится 30 минут<br>'+
        'На прохождение практики отводится 24 часа <br>'+
        'Общее время прохождения теста составляет 24 часа. Время прохождения тестов не суммируется. <br>'+
        'Благодарим Вас за то что решили пройти стажировку в нашей компании<br>'+
        'Команда технической поддержки<br><br><br>'+

        '<div style="color : #666666;">MIRUSDESK - Прогрессивные бухгалтерские решения<br>'+
        'тел.: +7 (727) 315-2152, моб.: +7 (701) 981-7860 <br>'+
        '<a href="http://www.mirusdesk.kz/" target="_blank" style="color : #666666;"> www.mirusdesk.kz</a> | <a href="mailto:support@mirusdesk.kz" target="_blank">support@<wbr>mirusdesk.kz</a>'+
        '</div></div>';


    sendEmailWithBody(html, item);
}


function sendRating(test) {
    var html = '';
    html = '<div>' +
        '<div style="text-align: center;">' +
        '<img src="http://invoice.mirusdesk.kz/images/logo.png"><br><br><br>' +
        'Уведомление для "'+test.name+'"<br>' +
        'Уведомление<br>'+
        'о прохождении тестрования</div><br><br>'+
        'Результаты тестирования</div><br><br>';
    html += 'Теория: ';
    test.testA.forEach(function (item) {
        html += 'Задача: '+item.title+'; Стоимость: ' + item.price + '; Оценка: ' + item.summ+'<br>';
    });
    html += '<br>Практика: ';
    test.testB.forEach(function (item) {
        html += 'Задача: '+item.title+'; Стоимость: ' + item.price + '; Оценка: ' + item.summ+'<br>';
    });
    html += '<br>'+
        'Благодарим Вас за то что решили пройти стажировку в нашей компании<br>'+
        'Команда технической поддержки<br><br><br>'+

        '<div style="color : #666666;">MIRUSDESK - Прогрессивные бухгалтерские решения<br>'+
        'тел.: +7 (727) 315-2152, моб.: +7 (701) 981-7860 <br>'+
        '<a href="http://www.mirusdesk.kz/" target="_blank" style="color : #666666;"> www.mirusdesk.kz</a> | <a href="mailto:support@mirusdesk.kz" target="_blank">support@<wbr>mirusdesk.kz</a>'+
        '</div></div>';

    console.log(html);


    sendEmailWithBody(html, test);
}

function sendEmailWithBody(body, item) {
    var transporter = nodemailer.createTransport('smtps://support@mirusdesk.kz:Z123456789z@smtp.gmail.com');

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: 'support@mirusdesk.kz', // sender address
        replyTo :'support@mirusdesk.kz',
        to: item.email, // list of receivers
        // 'ticket@mirusdesk.kz'
        subject: "Тесты по Бухгалтерии MD Company", // Subject line
        // text: 'Hello world 🐴', // plaintext body
        html: body
    };

// send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            // console.log(item.email);
            // sendEmail(item);
            return console.log(error);

        }
        console.log('Message sent: ' + info.response);
    });
}

app.get('/api/userTest', function (req, res, next) {

    UserTest.find({}).populate('test').exec( function(err, tests) {
        if (err) return next(err);

        res.status(200).send({ tests: tests, message: 'Success'});
    });
});

// отправка письма с оценками
app.post('/api/userTest/sendRating', function (req, res, next) {

    console.log(req.body);

    async.waterfall([
        function (done) {
            UserTest.findById(req.body._id).deepPopulate('testA.questions testB.questions').exec( function(err, test) {
                if (err) return next(err);
                console.log(test);
                sendRating(test);
                done(err, test);
            });
        }
    ], function(err, test){
        if(err) return next(err);
        res.status(200).send({test: test, message: 'Success'});
    });

});


app.get('/api/userTest/:id', function (req, res, next) {

    async.waterfall([
        function (done) {
            UserTest.findById(req.params.id).deepPopulate('testA.questions testB.questions').exec( function(err, test) {
                if (err) return next(err);
                if (req.query.start){
                    console.log(req.query.start);
                    if (req.query.start=='true'){
                        console.log('req.query.start');

                        console.log(test.startDate);
                        if (test.startDate == undefined) {
                            console.log('testStartDate');

                            test.startDate = new Date();
                            test.save(function (err) {
                                if (err)console.log(err);
                            });
                        }
                    }
                }
                done(err, test);
            });
        }
    ], function(err, test){
        if(err) return next(err);
        res.status(200).send({test: test, message: 'Success'});
    });

});

app.post('/api/userTest', function (req, res, next) {

    console.log(req.body);
    var test = new UserTest({
        name : req.body.name,
        email : req.body.email,
        test : req.body.test
    });
    console.log(test);
    Test.findById(req.body.test).deepPopulate('testA.questions testB.questions').exec( function(err, testA) {
        if (err) return next(err);

        if (test.testA.length == 0) {
            console.log('test A length 0');
            testA.testA.forEach(function (item) {
                var task = new Task({
                    type: item.type,
                    title: item.title,
                    description: item.description,
                    price: item.price,
                    questions: [],
                    columns : item.columns,
                    responsible : item.responsible,
                    file : item.file
                });
                item.questions.forEach(function (quest) {
                    var question = new Question({
                        type: quest.type, /** table, text, test, file */
                        question: quest.question,
                        answer: quest.answer,
                        answers: quest.answers,
                        columns: quest.columns
                    });
                    task.questions.push(question);
                    question.save(function (err) {
                        if (err)console.log(err);
                    });
                });
                task.save(function (err) {
                    if (err)console.log(err);
                });
                // console.log(task);
                test.testA.push(task);
            });
        }
        if (test.testB.length == 0) {
            console.log('test B length 0');
            testA.testB.forEach(function (item) {
                var task = new Task({
                    type: item.type,
                    title: item.title,
                    description: item.description,
                    price: item.price,
                    questions: [],
                    columns : item.columns,
                    responsible : item.responsible,
                    file : item.file
                });
                item.questions.forEach(function (quest) {
                    var question = new Question({
                        type: quest.type, /** table, text, test, file */
                        question: quest.question,
                        answer: quest.answer,
                        answers: quest.answers,
                        columns: quest.columns
                    });
                    task.questions.push(question);
                    question.save(function (err) {
                        if (err)console.log(err);
                    });
                });
                task.save(function (err) {
                    if (err)console.log(err);
                });
                // console.log(task);
                test.testB.push(task);
            });
        }

        test.save(function (err) {
            if (err)console.log(err);
            test.test = testA;
            sendEmail({
                email : test.email,
                nameStudent : test.name,
                link : config.get("link")+'/#/userTest/'+test._id+'?start'
            });
            res.status(200).send({test: test, message: 'Success'});
        });
    });

});

app.put('/api/userTest/:id', function (req, res, next) {

    UserTest.findById(req.params.id).exec( function(err, test) {
        if (err) return next(err);

        test.name = req.body.test.name;
        test.email = req.body.test.email;
        test.test = req.body.test.test;
        test.testA = req.body.test.testA;
        test.testB = req.body.test.testB;
        test.finishA= req.body.test.finishA;
        test.finishB = req.body.test.finishB;

        var arr = req.body.test.testA.concat(req.body.test.testB);
        async.map(arr, function (item, callback) {

            async.waterfall([
                function (done) {
                    Task.findById(item._id).exec( function(err, task) {
                        if (err) return next(err);

                        task.type = item.type;
                        task.title = item.title;
                        task.description = item.description;
                        task.price = item.price;
                        task.questions = item.questions;
                        task.columns = item.columns;
                        task.answer = JSON.stringify(item.answer);

                        task.save(function (err) {
                            if (err)console.log(err);
                            done(err, task);
                        });

                    });


                },
                function(task, done){
                    async.map(item.questions, function (item, callback) {

                        Question.findById(item._id).exec( function(err, quest) {
                            if (err) return next(err);
                            console.log(quest);
                            quest.type = item.type;
                            quest.question = item.question;
                            quest.answer = item.answer;
                            quest.answers = item.answers;
                            quest.columns = item.columns;
                            quest.save(function (err) {
                                if (err)console.log(err);
                                callback(null, quest);
                            });
                        });

                    }, function (err) {
                        done(null, task);
                    });
                }
            ], function(err, test){
                if(err) return next(err);
                callback(null, item);
            });



        }, function (err) {
            test.save(function (err) {
                if (err) return next(err);
                res.status(200).send({test: test, message:'Update'});
            });
        });


    });
});





module.exports = app;
