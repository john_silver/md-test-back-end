var express = require('express');
var Base64 = require('js-base64').Base64; // для шифрования почты в Base64
var URLSafeBase64 = require('urlsafe-base64');

var app = express();

var Task = require('../models/Task');


// Функция отдачи файла
app.get('/api/attachment/:id', function (req, res, next) {

    Task.findById(req.params.id).exec( function(err, task) {
        if (err) return next(err);
        if(task.answer) {
            var answer = JSON.parse(task.answer);
            var filename = '=?utf-8?B?' + Base64.encode(answer.filename) + '?=';
            res.set({"Content-Disposition": "attachment; filename=\"" + '=?utf-8?B?' + Base64.encode(answer.filename) + "\""});
            res.send(URLSafeBase64.decode(answer.base64));
        }else{
            res.send("Скорее всего, файл небыл прикреплен");
        }
    });
});

// Функция отдачи файла
app.get('/api/file/:id', function (req, res, next) {
    console.log('/api/file/:id');
    Task.findById(req.params.id).exec( function(err, task) {
        if (err) return next(err);
        if(task.file) {

            var answer = JSON.parse(task.file);
            console.log(answer.filename);
            var filename = '=?utf-8?B?' + Base64.encode(answer.filename) + '?=';
            res.set({"Content-Disposition": "attachment; filename=\"" + '=?utf-8?B?' + Base64.encode(answer.filename) + '?=' + "\""});
            res.send(URLSafeBase64.decode(answer.base64));
        }else{
            res.send("Скорее всего, файл небыл прикреплен");
        }
    });
});


module.exports = app;
