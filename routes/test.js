var express = require('express');

var app = express();

var Test = require('../models/Test');
var Task = require('../models/Task');
var Question = require('../models/Question');

app.get('/api/test', function (req, res, next) {

    Test.find({}).exec( function(err, tests) {
        if (err) return next(err);
        res.status(200).send({ tests: tests, message: 'Success'});
    });
});

app.get('/api/test/:id', function (req, res, next) {

    Test.findById(req.params.id).deepPopulate('testA.questions testB.questions').exec( function(err, test) {
        if (err) return next(err);
        res.status(200).send({ test: test, message: 'Success'});
    });
});


function addTest(testBase, testMongo,callback) {
    // console.log(testBase.testA);
    var test;
    if (testMongo==undefined) {
         test = new Test({
            name: testBase.name,
            testA: [],
            testB: []
        });
    }else{
        test = testMongo;
        test.name = testBase.name;
        test.testA = [];
        test.testB = [];
    }

    testBase.testA.forEach(function (item) {
        var task = new Task({
            type : item.type,
            title : item.title,
            description: item.description,
            price: item.price,
            questions : [],
            columns : item.columns,
            responsible : item.responsible,
            file : JSON.stringify(item.file)
        });
        item.questions.forEach(function (quest) {
            var question = new Question({
                type : quest.type,/** table, text, test, file */
                question : quest.question,
                answer : quest.answer,
                answers : quest.answers,
                columns : quest.columns
            });
            task.questions.push(question);
            question.save(function (err) {
                if (err)console.log(err);
            });
            // console.log(question);
        });
        task.save(function (err) {
            if (err)console.log(err);
        });
        // console.log(task);
        test.testA.push(task);
    });

    testBase.testB.forEach(function (item) {
        var task = new Task({
            type : item.type,
            title : item.title,
            description: item.description,
            price: item.price,
            questions : [],
            responsible : item.responsible,
            file : JSON.stringify(item.file)
        });
        item.questions.forEach(function (quest) {
            var question = new Question({
                type : quest.type,/** table, text, test, file */
                question : quest.question,
                answer : quest.answer,
                answers : quest.answers,
                columns : quest.columns
            });
            task.questions.push(question);
            question.save(function (err) {
                if (err)console.log(err);
            });
            console.log(question);
        });
        task.save(function (err) {
            if (err)console.log(err);
        });
        // console.log(task);
        test.testB.push(task);
    });

    test.save(function (err) {
        if (err)console.log(err);
        callback(err, test);
    });
}


app.post('/api/test', function (req, res, next) {

    // addTest()

    var test = new UserTest({
        name : req.body.name,
        testA : req.body.testA,
        testB : req.body.testB
    });
    test.save(function (err) {
        if (err) return next(err);
        res.status(200).send({test: test, message:'Success'});
    });
});

app.put('/api/test/:id', function (req, res, next) {
    if (req.params.id=='new'){
        req.params.id = undefined;
    }
    Test.findById(req.params.id).exec( function(err, test) {
        if (err) return next(err);
        if (test){
            addTest(req.body, test, function (err, test) {
                if (err) return next(err);
                res.status(200).send({test: test, message:'Update'});
            });
        }else{
            addTest( req.body, undefined, function (err, test) {
                if (err) return next(err);
                res.status(200).send({test: test, message:'Update'});
            });
        }

        // test.save(function (err, test) {
        //     if (err) return next(err);
        //     res.status(200).send({test: test, message:'Update'});
        // });
    });
});



module.exports = app;
