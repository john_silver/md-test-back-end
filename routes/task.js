var express = require('express');

var app = express();

// var Part = require('../models/Part');

var Task = require('../models/Task');

app.get('/api/task', function (req, res, next) {

    Task.find({}).exec( function(err, tasks) {
        if (err) return next(err);

        res.status(200).send({ tasks: tasks, message: 'Success'});
    });
});

app.get('/api/task/:id', function (req, res, next) {

    Task.findById(req.params.id).exec( function(err, task) {
        if (err) return next(err);

        res.status(200).send({ task: task, message: 'Success'});
    });
});

// app.post('/api/task', function (req, res, next) {
//
//     var task = new UserTest({
//         name : req.body.name,
//         testA : req.body.testA,
//         testB : req.body.testB
//     });
//     test.save(function (err) {
//         if (err) return next(err);
//         res.status(200).send({test: test, message:'Success'});
//     });
// });

app.post('/api/task/:id', function (req, res, next) {
    Task.findById(req.params.id).exec( function(err, task) {
        if (task.responsible==req.body.userId){
            task.summ = req.body.task.summ;
            task.save(function (err) {
                if (err)console.log(err);
                res.status(200).send({task: task, message: 'Success'});
            });
        }else{
            res.status(200).send({task: task, message: 'Error update. responsible not set'});
        }



    });

});

app.put('/api/test/:id', function (req, res, next) {

    Test.findById(req.params.id).exec( function(err, test) {
        if (err) return next(err);
        test.name = req.body.name;
        test.testA = req.body.testA;
        test.testB = req.body.testB;

        test.save(function (err) {
            if (err) return next(err);
            res.status(200).send({test: test, message:'Update'});
        });
    });
});



module.exports = app;




module.exports = app;


module.exports = app;
