var express = require('express');
var async = require('async');
var app = express();


var User = require('../models/User');

// Добавление
app.put('/api/user', function(req, res, next) {
    async.waterfall([
        function( done) {
            var user = new User({
                // regular
                name: req.body.name,
                password: req.body.idToEnter,
                idToEnter: req.body.idToEnter,
                telephone: req.body.telephone,
                email: req.body.email,
                role: 1
            });
            user.save(function(err) {
                if (err) return next(err);
                res.status(200).send({ user: user, message: 'Success'});
            });
        }
    ])

});


// изменение
app.post('/api/user/:id', function(req, res, next) {
    // todo: добавление массива keywords
    async.waterfall([
        function (done) {
            User.findById(req.params.id, function(err, user) {
                if (err) return next(err);
                done(null, user);
            });
        },
        function(user, done) {
            user.name = req.body.name;
            // user.password = req.body.password;
            user.idToEnter = req.body.idToEnter;
            user.telephone = req.body.telephone;
            user.email = req.body.email;
            // user.role = req.body.role;

            user.save(function(err) {
                if (err) return next(err);
                done(err, user);
            });
        }
    ], function(err, user){
        if(err) return next(err);
        res.status(200).send({ user: user, message: 'Success'});
    });

});

app.get('/api/user/:id', function (req, res, next) {

    User.findById(req.params.id, function(err, user) {
        if (err) return next(err);
        res.status(200).send({ user: user, message: 'Success'});
    });

});


// список
app.get('/api/user', function (req, res, next) {

    User.find({}).exec(function(err, result){
        if (err) return next(err);
        res.status(200).send({ users: result, message: 'Success'});
    });

});


module.exports = app;
