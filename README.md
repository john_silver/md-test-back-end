# README #

Back-end часть проекта MD Test

## Включает в себя: ##

* Help Desk API
* Client API
* User API

## Модели ##

* ### Client ###
* ### User ###
* ### Comment ###
* ### Header ###
* ### KeyWord ###
* ### Part ###
* ### Payload ###
* ### Thread ###
* ### Ticket ###


## Routing ##

* auth
* user
* client
* comment
* help
* keyWord
* thread
* ticket

## Cron ##

* ### closeTicket ###
файл запускается каждые 60 минут и закрывает тикеты, которые находились в статусе "Ожидание больше 24-х часов"

* ### getTciketFromServer ###
получает все письма за последние сутки, проверяет каждую минуту


#confing#

* default (production)
* development (npm start)

#move database to server#
* mongodump  --db ticketPlan
* scp -r dump root@159.203.234.82:/opt/
* mongorestore -d ticketPlan ticketPlan