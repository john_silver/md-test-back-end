var mongoose = require('mongoose');

// user schema
var taskSchema = new mongoose.Schema({
    // regular
    type: String, /** theory, practical, table, asFile */
    title : String,
    description : String,
    price : String,
    answer : String,
    file : String,
    summ : String, // сумма за ответ
    questions:  [{ type: mongoose.Schema.ObjectId, ref: 'Question' }],
    columns : [String],
    responsible : {type : mongoose.Schema.ObjectId, ref: 'User'}
});

module.exports = mongoose.model('Task', taskSchema);