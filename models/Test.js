var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);

// user schema
var testSchema = new mongoose.Schema({
    // regular
    name : String,
    testA:  [{ type: mongoose.Schema.ObjectId, ref: 'Task' }],
    testB : [{ type: mongoose.Schema.ObjectId, ref: 'Task' }]

});

testSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Test', testSchema);