var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);

// user schema
var userTestSchema = new mongoose.Schema({
    // regular
    // for table
    name : String,
    email : String,
    test : {type: mongoose.Schema.ObjectId, ref: 'Test'},
    testA:  [{ type: mongoose.Schema.ObjectId, ref: 'Task' }],
    testB : [{ type: mongoose.Schema.ObjectId, ref: 'Task' }],
    createDate : { type: Date, default: Date.now },
    startDate : Date,
    finishDate : Date,
    finishA : {type: Boolean, default: false},
    finishB : {type: Boolean, default: false}
});

userTestSchema.plugin(deepPopulate, {
    populate: {
        'testA': {
            select: 'type title description price questions columns responsible summ'
        },
        'testB': {
            select: 'type title description price questions columns responsible summ'
        }
    }
});

module.exports = mongoose.model('UserTest', userTestSchema);