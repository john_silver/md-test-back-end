var mongoose = require('mongoose');

// user schema
var questionSchema = new mongoose.Schema({
    // regular
    // for table
    type : String,/** table, text, test, file */
    question : String,
    answer : String,
    answers : [String],
    columns : [String]
});



module.exports = mongoose.model('Question', questionSchema);